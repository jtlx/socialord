from django.db.models import get_models

for model in get_models():
   # Do something with your model here
   print model.__name__, [x.name for x in model._meta.fields]