from django.db import models
from django_facebook.models import FacebookCustomUser
from django.forms import ModelForm, DateField


class UserData(models.Model):
	Serving = models.BooleanField()
	Enhanced = models.BooleanField(blank=True)

	ORDDate = models.DateField(blank=True, null=True)
	user = models.OneToOneField(FacebookCustomUser, related_name='orddata')
	def __str__(self):
		return str(self.user)

class UserDataForm(ModelForm):
	class Meta:
		model = UserData
		fields = ['Serving','Enhanced','ORDDate']
