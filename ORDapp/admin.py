from django.contrib import admin

# Register your models here.
from django.contrib.auth.models import User
from django_facebook.models import FacebookCustomUser
from django.db import models
from ORDapp.models import UserData

admin.site.register(UserData)
admin.site.register(FacebookCustomUser)

