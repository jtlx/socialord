from django import template
from datetime import date
from dateutil.relativedelta import relativedelta

register = template.Library()

def daysuntil(ORDDate):
	deltatillord = ORDDate - date.today()

	if deltatillord.days < 0:
		display_value = "ORD"
	else:
		display_value = str(deltatillord.days) + " DAYS"

	return display_value

def percentuntil(ORDDate, arg):
	if arg == True:
		# enlistdate=ORDDate-relativedelta(years=1,months=10)
		service_term=30.4*22
	else:
		# enlistdate=ORDDate-relativedelta(years=2)
		service_term=30.4*24

	deltatillord = ORDDate - date.today()
	days_served = service_term - deltatillord.days

	percentage_served = days_served/service_term * 100

	percentage_served = round(percentage_served,1)

	if percentage_served < 100:
		display_value = str(percentage_served) + "%"
	else:
		display_value = "ORD"

	return display_value

register.filter('daysuntil', daysuntil)
register.filter('percentuntil', percentuntil)