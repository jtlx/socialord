from django.shortcuts import render
from open_facebook.api import OpenFacebook
from django.http import HttpResponseRedirect, HttpResponse
from django.db import models 
from django.dispatch.dispatcher import receiver 
from django_facebook.models import FacebookModel, FacebookProfile, BaseFacebookModel, FacebookCustomUser, FacebookUser
from django.db.models.signals import post_save 
from django_facebook.utils import get_user_model, get_profile_model 
from SocialORD import settings
from ORDapp.models import UserData,UserDataForm
from django.db.models import get_app, get_models, Count
from django.template import RequestContext, loader
from django.contrib.auth.decorators import login_required
from django_facebook.api import FacebookUserConverter
from open_facebook.api import OpenFacebook
from dateutil.relativedelta import relativedelta
from datetime import date
from django_facebook.decorators import facebook_required_lazy, facebook_required
from django.contrib import messages
from django.core.urlresolvers import reverse
import json


# Create your views here.
# @facebook_required(scope='publish_stream')
@login_required(login_url='/login/')
def index(request):
	daystillord = "nil"
	daysofservice = "nil"
	current_user = request.user

	if hasattr(current_user, 'orddata'):
		userdata_exists=True
	else:
		userdata_exists=False

	graph=OpenFacebook(current_user.facebookprofile.access_token)
	# graph.set('me/feed', message='tesdsat', url='http://www.google.com')

	json_friends=graph.get('v1.0/me/friends')

	json_friends_ids=[x['id'] for x in json_friends['data']]
	# json_test=str(json_friends)

	# json_extract=json_friends

	# friend_dict=json.loads(json_friends)
	# friend_dict=json.loads(json_test)
	# json_test=str(friend_dict)


	if userdata_exists==True and current_user.orddata.Serving == True:
		if current_user.orddata.Enhanced == True:
			enlist_date = current_user.orddata.ORDDate - relativedelta(years=1,months=10)
		else:
			enlist_date = current_user.orddata.ORDDate - relativedelta(years=2)

	else:
		enlist_date = None




	friends = FacebookUser.objects.filter(user_id = current_user.facebookprofile.user.id)
	users = FacebookProfile.objects.all()


	#get friends who are users of the site
	mutuals_set = set()
	for friend in json_friends_ids:
		for user in users:
			if int(friend) == user.facebook_id:
				mutuals_set.add(user.pk)


	mutuals = FacebookProfile.objects.filter(pk__in=mutuals_set)

	#filter friends on app to friends on app who input an ORD date
	mutuals_serving_set = set()
	for mutual in mutuals:
		try:
			mutual.user.orddata.ORDDate
			mutuals_serving_set.add(mutual.pk)
		except:
			pass


	#days left till ORD
	try:
		deltatillord = current_user.orddata.ORDDate - date.today()
		daystillord = deltatillord.days
	except:
		daystillord = 0

	#total days soldier needs to serve NS
	try:
		deltaofservice = enlist_date - current_user.orddata.ORDDate
		daysofservice = deltaofservice.days
	except:
		daysofservice = 0
	
	#add self to friend table for comparison
	try:
		current_user.orddata.ORDDate
		mutuals_serving_set.add(current_user.facebookprofile.pk)
	except:
		pass
		
# refernce code #
#	 {% for mutual in mutuals_serving %}
#	 {% if mutual.user.orddata.ORDDate %}
#	{{ mutual.user.orddata.ORDDate }}
# refernce code #

	mutuals_serving = FacebookProfile.objects.filter(pk__in=mutuals_serving_set)

	mutuals_serving = mutuals_serving.order_by('user__orddata__ORDDate')

	context = {'mutuals': mutuals,'UserData':UserData,'current_user':current_user,'userdata_exists':userdata_exists,'UserDataForm':UserDataForm,'mutuals_serving':mutuals_serving,'enlist_date':enlist_date,'daystillord':daystillord,'daysofservice':daysofservice}
	return render(request, 'ORDapp/index.html', context)


def login(request):
	if request.user.is_authenticated():
		return HttpResponseRedirect('/')
	else:
		return render(request, 'ORDapp/login.html')

def register(request):
	Serving = True
	# Enhanced = request.POST['Enhanced']
	Enhanced = request.POST.get('Enhanced', False)
	ORDDate = request.POST['ORDDate']
	current_user = request.user

	r = UserData(Enhanced=Enhanced,Serving=Serving,ORDDate=ORDDate,user=current_user)
	try:
		r.save()
		return HttpResponseRedirect('/')
	except:
		# return HttpResponse("An error occourred. sian")
		messages.error(request, 'Did you enter a date?')
		return HttpResponseRedirect(reverse('index'))

def spectator_register(request):
	
	current_user = request.user
	r = UserData(Enhanced=False,Serving=False,ORDDate=None,user=current_user)
	try:
		r.save()
		return HttpResponseRedirect('/')
	except Exception, e:
		return HttpResponse(str(e))

def reset(request):
	current_user = request.user
	r = current_user.orddata
	r.delete()

	return HttpResponseRedirect('/')

def refresh_friends(request):
	current_user = request.user
	try:
		FacebookUserConverter.get_and_store_friends(current_user)
		return HttpResponseRedirect('/')
	except Exception, e:
		return HttpResponse(str(e))



