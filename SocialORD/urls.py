from django.conf.urls import patterns, include, url

from django_facebook import connect
from ORDapp import views
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	url(r'^$', include('ORDapp.urls')),
	url(r'^login/$', views.login, name='login'),
	url(r'^register/$', views.register, name='register'),
	url(r'^reset/$', views.reset, name='reset'),
	url(r'^spectator_register/$', views.spectator_register, name='spectator_register'),
	url(r'^refresh_friends/$', views.refresh_friends, name='refresh_friends'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^facebook/', include('django_facebook.urls')),
	url(r'^accounts/', include('django_facebook.auth_urls')), #Don't add this line if you use django registration or userena for registration and auth.
)
